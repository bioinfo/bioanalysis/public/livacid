#if (!require("BiocManager", quietly = TRUE))
#  install.packages("BiocManager")
#BiocManager::install("DESeq2")
#if (!require("BiocManager", quietly = TRUE))
#    install.packages("BiocManager")
#BiocManager::install("apeglm")
#if (!require("BiocManager", quietly = TRUE))
#    install.packages("BiocManager")
#BiocManager::install("topGO")

#install.packages("gplots")
#install.packages("rlang", force=TRUE)
#install.packages("dplyr")
#install.packages("VennDiagram")

options(warn=-1) #deactivate this line if you need to see warnings
library(DESeq2, verbose=FALSE, quietly=TRUE)
library(ggplot2, verbose=FALSE, quietly=TRUE)
library(pheatmap, verbose=FALSE, quietly=TRUE)
library(gridExtra, verbose=FALSE, quietly=TRUE)
library(grid, verbose=FALSE, quietly=TRUE)
library(rafalib, verbose=FALSE, quietly=TRUE)
library(RColorBrewer, verbose=FALSE, quietly=TRUE)
library(gplots, verbose=FALSE, quietly=TRUE)
library(apeglm, verbose=FALSE, quietly=TRUE)
library(scales, verbose=FALSE, quietly=TRUE)
library(VennDiagram, verbose=FALSE, quietly=TRUE)
#library(topGO, verbose=FALSE, quietly=TRUE)

working_dir <- "OUTPUT/DEG"
star <- "OUTPUT/star"
target <- "DATA/target_col4.txt"
function_file <- "SRC/R/functions.r"
go_file <- "DATA/dicLab1_scaffold.gene2go"
ann_file <- "DATA/dicLab1_scaffold.b2g"

featuresToRemove <- c("N_unmapped","N_multimapping","N_noFeature","N_ambiguous")
threshold <- 10 #min expression value accross samples
minNumberOfReads <- 20000000

design1 <- counts ~ group
design2 <- counts ~ sampling_day + group 
design3 <- counts ~ sampling_day + group + sampling_day:group
design4 <- counts ~ sampling_day

design <- design2
filter <- "filterb"


# Change working directory
setwd(working_dir)  
# Source R function file
source(function_file)
# Load input data
curdir <- star        
sampleSheet <- target
samplesInfo <- read.table(sampleSheet, h=T) 
target_file <- samplesInfo[,c(1,2,3,4)]

target_file$group <- factor(target_file$group, levels=c("control","acidity"))
target_file$sampling_day <- factor(target_file$sampling_day, levels=c("J1","J2"))
design <- formula(design)

target_file
print(paste("The input datatable is composed of ",dim(target_file)[1]," lines and ", dim(target_file)[2], " columns", sep=""))

#Import count data 
if (design == design1) {
    ddsHTSeq <- DESeqDataSetFromHTSeqCount(sampleTable = target_file, directory = star, design=~group)
} else if (design == design2) {
    ddsHTSeq <- DESeqDataSetFromHTSeqCount(sampleTable = target_file, directory = star, design=~sampling_day+group)
} else if (design == design3) {
    ddsHTSeq <- DESeqDataSetFromHTSeqCount(sampleTable = target_file, directory = star, design=~sampling_day+group)+group:sampling_day
}
#Trims unwanted features
wanted <- setdiff(rownames(ddsHTSeq), featuresToRemove)
ddsHTSeq <- ddsHTSeq[wanted,] 

#Trims too low transcripts
if (filter == "filtera") {
    ddsHTSeq <- ddsHTSeq[ rowSums(counts(ddsHTSeq)) > threshold, ]
} else if (filter == "filterb") {
    ddsHTSeq <- ddsHTSeq[ rowSums(counts(ddsHTSeq) >= threshold) >= dim(target_file)[1]/2, ]
}

#Look at resulting object
print("This is DESeq object containing count data")
ddsHTSeq


# Explore count data
sample_list <- target_file$group
sample_color <- ifelse(sample_list=="control","turquoise3","indianred2")
head(counts(ddsHTSeq))
summary(counts(ddsHTSeq))
boxplot(log(counts(ddsHTSeq),2), col=sample_color, las=2, main="Distribution of raw counts accross samples (log2)\nblue samples are control samples", warning=F)


barplot(colSums(counts(ddsHTSeq)), col=sample_color, las=2, ylim=c(0,80000000), main="Total number of raw counts per sample\nblue samples are control samples")


barplot(colSums(counts(ddsHTSeq)<5), col=sample_color, las=2, ylim=c(0,6000), main="Number of transcripts associated with\n less than 5 read counts")


# Get counts to plot counts distribution for each sample
ct <- counts(ddsHTSeq)
d <- stack(data.frame(ct))
group <- target_file$group
d$group <- rep(group, each=nrow(ct))
dp_raw_all <- ggplot(d, aes(x=.data$values+1)) +
    stat_density(aes(group=.data$ind, color=.data$group), position="identity", geom="line", show.legend=TRUE) +
    scale_colour_manual(values=c("turquoise3","indianred2")) +
    scale_x_continuous(trans = log10_trans(),
                               breaks = trans_breaks("log10", function(x) 10^x))+#,
                               #labels = trans_format("log10", math_format(~10^.x))) +
    labs(color="") +
    #scale_colour_manual(values=d$color) +
    xlab("Raw counts") +
    ylab("Density") +
    ggtitle("Density of raw counts distribution for all genes") +
    theme_gray()
dp_raw_all

#DEseq object
ddsp <- DESeq(ddsHTSeq, fitType="parametric")
dds <- DESeq(ddsHTSeq, fitType="local")

# Get counts to plot counts distribution for each sample
ct <- counts(dds, normalized=TRUE)
d <- stack(data.frame(ct))
group <- target_file$group
d$group <- rep(group, each=nrow(ct))
dp_norm_all <- ggplot(d, aes(x=.data$values+1)) +
    stat_density(aes(group=.data$ind, color=.data$group), position="identity", geom="line", show.legend=TRUE) +
    scale_x_continuous(trans = log10_trans(),
                               breaks = trans_breaks("log10", function(x) 10^x))+#,labels = trans_format("log10", math_format(~10^.x))) +
    labs(color="") +
    scale_colour_manual(values=c("turquoise3","indianred2")) +
    xlab("Raw counts") +
    ylab("Density") +
    ggtitle("Density of normalized counts distribution for all genes") +
    theme_gray()
dp_norm_all

rld <- rlog(ddsHTSeq, blind=FALSE)
data <- plotPCA(rld, intgroup=c("group","sampling_day"), returnData=TRUE)#, ntop=100000)
data
percentVar <- round(100*attr(data, "percentVar"))
pca1 <- ggplot(data, aes(PC1, PC2, color=group.1, shape=sampling_day)) +
  geom_point(size=3.5) +#, fill="black", stroke=0.8) +
  xlab(paste0("PC1: ",percentVar[1],"% variance")) +
  ylab(paste0("PC2: ",percentVar[2],"% variance")) +
  coord_fixed() + ggtitle("Principal Component Analysis plot") +
  geom_hline(aes(yintercept=0)) +
  geom_vline(aes(xintercept=0)) +
  scale_shape_manual(values=c(21,24)) +
  scale_color_manual(values=c("turquoise3","indianred2")) +
  geom_text(aes(label=colnames(ddsHTSeq)),hjust=0.5, vjust=1.8, size=4)
pca1


data <- plotPCA.san(rld, intgroup=c("group","sampling_day"), returnData=TRUE)#, ntop=100000)
percentVar <- round(100*attr(data, "percentVar"))
pca2 <- ggplot(data, aes(PC1, PC3, color=group.1, shape=sampling_day)) +
  geom_point(size=3.5) +#, fill="black", stroke=0.8) +
  xlab(paste0("PC1: ",percentVar[1],"% variance")) +
  ylab(paste0("PC3: ",percentVar[3],"% variance")) +
  coord_fixed() + ggtitle("Principal Component Analysis plot") +
  geom_hline(aes(yintercept=0)) +
  geom_vline(aes(xintercept=0)) +
  scale_shape_manual(values=c(21,24)) +
  scale_color_manual(values=c("turquoise3","indianred2")) +
  geom_text(aes(label=colnames(ddsHTSeq)),hjust=0.5, vjust=1.8, size=4)
pca2

#Heatmap
#Selects only most abundant transcripts
nCounts <- counts(dds, normalized=TRUE)
select <- order(rowMeans(nCounts),decreasing=TRUE)[1:100]

#Selects corresponding norm counts
nt <- normTransform(dds)
log2.norm.counts <- assay(nt)[select,]
log2.norm.counts.to.export <- assay(nt)
colnames(log2.norm.counts.to.export) <- colnames(ddsHTSeq)
dim(log2.norm.counts.to.export)

#Gets the metadata
colnames(log2.norm.counts) <- colnames(ddsHTSeq)
df <- as.data.frame(colData(dds)["group"])
rownames(df) <- colnames(ddsHTSeq)

#Heatmap parameters
dist1 <- "euclidean"
clust <- "ward.D"
ann_colors = list(Color = c(Acidity = "indianred2", Control = "turquoise3"))

## 'Overwrite' default draw_colnames with your own version 
assignInNamespace(x="draw_colnames", value="draw_colnames_45",ns=asNamespace("pheatmap"))

#Draw heatmap
pheatmap(log2.norm.counts,
         clustering_distance_cols = dist1, 
         clustering_method = clust, 
         cluster_rows=FALSE,
         cluster_cols=TRUE, 
         annotation_col=df,
         show_rownames=TRUE,
         fontsize_row=5,
         fontsize_col=10,
         fontsize=8,
         annotation_colors = ann_colors, 
         main=paste("Clustered heatmap of 100 most abundant genes\n",dist1," distance with ",clust, " clustering method",sep=""),
         las=1)



#Clustering
#Normalized counts
nt <- normTransform(dds)
log2.norm.counts <- assay(nt)

#Gets the metadata
colnames(log2.norm.counts) <- colnames(ddsHTSeq)
df <- as.data.frame(colData(dds)["group"])
rownames(df) <- colnames(ddsHTSeq)

#Heatmap parameters
dist1 <- "euclidean"
clust <- "ward.D"

#Build distance matrix
distance_matrix <- dist(t(log2.norm.counts), method = dist1)
hh <- hclust(distance_matrix, method = clust)
colo <- target_file[target_file$label==hh$labels,3]
colo <- ifelse(colo=="control","turquoise3","indianred2")

#plot clustering
myplclust(hh, labels=rownames(df), lab.col=colo)


#Heatmap sample to sample
distsRL <- dist(t(assay(rld)))
mat<- as.matrix(distsRL)
rownames(mat) <- colnames(mat) <- with(colData(dds), paste(colnames(ddsHTSeq), group , sep=' : '))
hc <- hclust(distsRL)
hmcol <- colorRampPalette(brewer.pal(9, "GnBu"))(100)

heatmap.2(mat, Rowv=as.dendrogram(hc),
          symm=TRUE, trace='none',
          col=rev(hmcol),margin=c(10, 10), colRow=colo, colCol=colo)


#Plot dispersion plot
plotDispEsts(dds)

## DE analysis
resultsNames(dds)
# Setting contrasts
contrast <- c("group", "acidity", "control")
    # DE test with BH correction and alpha <0.05
    res_raw <- results(dds, contrast = contrast, alpha = 0.01, pAdjustMethod = "BH")
    # lfc shrinkage
    res <- lfcShrink(dds=dds, coef="group_acidity_vs_control", type="apeglm", quiet=TRUE)
if (design == "counts ~ sampling_day") {
    contrast <- c("sampling_day", "J1", "J2")
    # DE test with BH correction and alpha <0.05
    res_raw <- results(dds, contrast = contrast, alpha = 0.01, pAdjustMethod = "BH")
    # lfc shrinkage
    res <- lfcShrink(dds=dds, coef="sampling_day_J2_vs_J1", type="apeglm", quiet=TRUE)
}

## MA plots 
plotMA.DESeqResults(res_raw, main="Acidity vs. Control\nbefore lfc shrinkage", ylim=c(-10,10),alpha=0.05)
plotMA.DESeqResults(res, main="Acidity vs. Control\nafter apeglm shrinkage", ylim=c(-10,10),alpha=0.05)

res <- as.data.frame(res)
res$geneName <- rownames(res)

#Selects results with p-value <= 0.01 
res0.01 <- res[which(res$padj<=0.01),] ;  #dim(res0.01)[1]
nbUp0.01 <- dim(res0.01[which(res0.01$log2FoldChange>0),])[1]
nbDown0.01 <- dim(res0.01[which(res0.01$log2FoldChange<0),])[1]

#Selects results with p-value <= 0.05 
res0.05 <-res[which(res$padj<=0.05),] ;  #dim(res0.05)[1]
nbUp0.05 <- dim(res0.05[which(res0.05$log2FoldChange>0),])[1]
nbDown0.05 <- dim(res0.05[which(res0.05$log2FoldChange<0),])[1]

#Apply a filter on log2FC (>1.5)
res0.01_1.5 <-res0.01[which(abs(res0.01$log2FoldChange) >= 1.5),] ; #dim(res0.01_1.5)[1]
nbUp0.01_1.5 <- dim(res0.01_1.5[which(res0.01_1.5$log2FoldChange>0),])[1]
nbDown0.01_1.5 <- dim(res0.01_1.5[which(res0.01_1.5$log2FoldChange<0),])[1]

res0.05_1.5 <-res0.05[which(abs(res0.05$log2FoldChange) >= 1.5),] ; #dim(res0.05_1.5)[1]
nbUp0.05_1.5 <- dim(res0.05_1.5[which(res0.05_1.5$log2FoldChange>0),])[1]
nbDown0.05_1.5 <- dim(res0.05_1.5[which(res0.05_1.5$log2FoldChange<0),])[1]

#Print number of DE genes
print(paste("Number of differentially expressed genes in control versus acidity (adjusted pvalue < 0.01) : ", dim(res0.01)[1], ", including ", nbUp0.01, " up-regulated genes and ", nbDown0.01, " down-regulated genes.", sep=""))
print(paste("Number of differentially expressed genes in control versus acidity (adjusted pvalue < 0.05) : ", dim(res0.05)[1], ", including ", nbUp0.05, " up-regulated genes and ", nbDown0.05, " down-regulated genes.", sep=""))
print(paste("Number of differentially expressed genes in control versus acidity (adjusted pvalue < 0.01 and |log2FC| > 1.5 ) : ", dim(res0.01_1.5)[1], ", including ", nbUp0.01_1.5, " up-regulated genes and ", nbDown0.01_1.5, " down-regulated genes.", sep=""))
print(paste("Number of differentially expressed genes in control versus acidity (adjusted pvalue < 0.05 and |log2FC| > 1.5 ) : ", dim(res0.05_1.5)[1], ", including ", nbUp0.05_1.5, " up-regulated genes and ", nbDown0.05_1.5, " down-regulated genes.", sep=""))

# Get counts to plot counts distribution for each sample
ct <- counts(ddsHTSeq[rownames(res0.01),])
d <- stack(data.frame(ct))
group <- target_file$group
d$group <- rep(group, each=nrow(ct))
dp_raw_DE0.01 <- ggplot(d, aes(x=.data$values+1)) +
    stat_density(aes(group=.data$ind, color=.data$group), position="identity", geom="line", show.legend=TRUE) +
    scale_x_continuous(trans = log10_trans(),
                               breaks = trans_breaks("log10", function(x) 10^x),
                               labels = trans_format("log10", math_format(~10^.x))) +
    labs(color="") +
    #scale_colour_manual(values=d$color) +
    xlab("Raw counts") +
    ylab("Density") +
    scale_colour_manual(values=c("turquoise3","indianred2")) +
    ggtitle("Density of raw counts distribution for DE genes (adj pv<0.01)") +
    theme_gray()
dp_raw_DE0.01

# Get counts to plot counts distribution for each sample
ct <- counts(dds[rownames(res0.01),], normalized=TRUE)
d <- stack(data.frame(ct))
group <- target_file$group
d$group <- rep(group, each=nrow(ct))
dp_norm_DE0.01 <- ggplot(d, aes(x=.data$values+1)) +
    stat_density(aes(group=.data$ind, color=.data$group), position="identity", geom="line", show.legend=TRUE) +
    scale_x_continuous(trans = log10_trans(),
                               breaks = trans_breaks("log10", function(x) 10^x),
                               labels = trans_format("log10", math_format(~10^.x))) +
    labs(color="") +
    scale_colour_manual(values=c("turquoise3","indianred2")) +
    xlab("Raw counts") +
    ylab("Density") +
    ggtitle("Density of normalized counts distribution for DE genes (adj pv<0.01)") +
    theme_gray()
dp_norm_DE0.01

head(res0.01)

pf0.01 <- read.csv(paste(working_dir,"OUTPUT/DEG/filterA/DE-genes_ctrl-vs-acid_pv0.01.txt", sep=""), sep="\t", h=T)$geneName
pf0.05 <- read.csv(paste(working_dir,"OUTPUT/DEG/filterA/DE-genes_ctrl-vs-acid_pv0.05.txt", sep=""), sep="\t", h=T)$geneName

print(paste("Using filterA to filter out low expressed transcripts, ",length(pf0.01)," DEG are retrieved (adj pv < 0.01) ; using filterB ",length(res0.01$geneName),"  DEG are retrieved (adj pv < 0.01)", paste=""))
inter1 <- intersect(res0.01$geneName,pf0.01)
print(paste("These 2 DEG datasets have ",length(inter1)," DEG in common", sep=""))
print("Here are the names of genes present in DEG filterA dataset but not in DEG filterB dataset")      
setdiff(pf0.01,res0.01$geneName)
print("Here are the names of genes present in DEG filterB dataset but not in DEG filterA dataset")      
setdiff(res0.01$geneName,pf0.01)
draw.pairwise.venn(length(pf0.01),length(res0.01$geneName),length(inter1),category=c("DEG filterA","DEG filterB"),
                   fill=c("darkseagreen","purple"), cat.col=c("darkseagreen","purple"), cat.cex=1.8, cat.pos=c(-30,30), 
                   cat.dist=c(0.05,0.05), quiet=TRUE)


print(paste("Using filterA to filter out low expressed transcripts, ",length(pf0.05)," DEG are retrieved (adj pv < 0.05) ; using filterB ",length(res0.05$geneName),"  DEG are retrieved (adj pv < 0.05)", paste=""))
inter2 <- intersect(res0.05$geneName,pf0.05)
print(paste("These 2 DEG datasets have ",length(inter2)," DEG in common", sep=""))
print("Here are the names of genes present in DEG filterA dataset but not in DEG filterB dataset")      
setdiff(pf0.05,res0.05$geneName)
print("Here are the names of genes present in DEG filterB dataset but not in DEG filterA dataset")      
setdiff(res0.05$geneName,pf0.05)
draw.pairwise.venn(length(pf0.05),length(res0.05$geneName),length(inter2),category=c("DEG filterA","DEG filterB"),
                   fill=c("darkseagreen","purple"), cat.col=c("darkseagreen","purple"), cat.cex=1.8, cat.pos=c(-30,30), 
                   cat.dist=c(0.05,0.05), quiet=TRUE)

go <- read.table(go_file, h=F, sep="\t") ; head(go)
ann <- read.csv(ann_file, h=T, sep="\t") ; head(ann)

resa <- merge(res, ann, by.x="geneName", by.y="Seq..Name") 
colnames(resa) <- c('geneName','baseMean','log2FoldChange','lfcSE','pvalue','padj','seqDescription','seqLength','nbHits','min_eValue','meanSimilarity','nbGOs','GOs','enzymeCodes','interProScan','symbol')
print("First line of results table")
resa[1,]

res0.01a <- merge(res0.01, ann, by.x="geneName", by.y="Seq..Name") 
colnames(res0.01a) <- c('geneName','baseMean','log2FoldChange','lfcSE','pvalue','padj','seqDescription','seqLength','nbHits','min_eValue','meanSimilarity','nbGOs','GOs','enzymeCodes','interProScan','symbol')

res0.05a <- merge(res0.05, ann, by.x="geneName", by.y="Seq..Name"); 
colnames(res0.05a) <- c('geneName','baseMean','log2FoldChange','lfcSE','pvalue','padj','seqDescription','seqLength','nbHits','min_eValue','meanSimilarity','nbGOs','GOs','enzymeCodes','interProScan','symbol')

res0.01_1.5a <- merge(res0.01_1.5, ann, by.x="geneName", by.y="Seq..Name") 
colnames(res0.01_1.5a) <- c('geneName','baseMean','log2FoldChange','lfcSE','pvalue','padj','seqDescription','seqLength','nbHits','min_eValue','meanSimilarity','nbGOs','GOs','enzymeCodes','interProScan','symbol')

res0.05_1.5a <- merge(res0.05_1.5, ann, by.x="geneName", by.y="Seq..Name") 
colnames(res0.05_1.5a) <- c('geneName','baseMean','log2FoldChange','lfcSE','pvalue','padj','seqDescription','seqLength','nbHits','min_eValue','meanSimilarity','nbGOs','GOs','enzymeCodes','interProScan','symbol')

# Function to get list of GOs from GO file
getGOlist <- function(dt,go) {
    for(i in 1:dim(dt)[1]) {
        dt[i,"GO_list"] <- paste(as.vector(go[which(go$V1==dt[i,"geneName"]),]$V2), collapse = ';')
    }
    return(dt)
}
 
# Add information to result data tables
resag <- getGOlist(resa, go)
res0.01ag <- getGOlist(res0.01a, go)
res0.05ag <- getGOlist(res0.05a, go)
res0.01_1.5ag <- getGOlist(res0.01_1.5a, go)
res0.05_1.5ag <- getGOlist(res0.05_1.5a, go)

allGenes <- as.vector(res$padj)
names(allGenes) <- res$geneName
geneIDs = res0.01ag[c(1,17)]
geneIDs
sampleGOdata <- new("topGOdata", description = "Simple session", 
                    ontology = "BP", allGenes = allGenes, geneSel = geneIDs,
                    nodeSize = 10, annot = annFUN.db, affyLib = affyLib)

inUniverse = geneIDs %in% c(anSig$ensembl_gene_id,  backG)
inSelection =  geneIDs %in% anSig$ensembl_gene_id

resag[c(1,2),]

png(paste(working_dir,"/images/boxplot_raw-counts.png", sep=""))
boxplot(log(counts(ddsHTSeq),2), col=sample_color, las=2, main="Distribution of raw counts accross samples (log2)\nblue samples are control samples", warning=F)
dev.off()

png(paste(working_dir,"/images/barplot_total-counts.png", sep=""))
barplot(colSums(counts(ddsHTSeq)), col=sample_color, las=2, ylim=c(0,80000000), main="Total number of raw counts per sample\nblue samples are control samples")
dev.off()

png(paste(working_dir,"/images/barplot_null-counts.png", sep=""))
barplot(colSums(counts(ddsHTSeq)<5), col=sample_color, las=2, ylim=c(0,6000), main="Number of transcripts associated with\n less than 5 read counts")
dev.off()


png(paste(working_dir,"/images/densityplot_raw-all.png", sep=""))
dp_raw_all
dev.off()

png(paste(working_dir,"/images/densityplot_norm-all.png", sep=""))
dp_norm_all
dev.off()

png(paste(working_dir,"/images/densityplot_norm-DE0.01.png", sep=""))
dp_norm_DE0.01
dev.off()

png(paste(working_dir,"/images/densityplot_raw-DE0.01.png", sep=""))
dp_raw_DE0.01
dev.off()


png(paste(working_dir,"/images/pca_pc1_pc2.png", sep=""))
pca1
dev.off()

png(paste(working_dir,"/images/pca_pc1_pc3.png", sep=""))
pca2
dev.off()

png(paste(working_dir,"/images/clustering_samples.png", sep=""))
myplclust(hh, labels=rownames(df), lab.col=colo)
dev.off()

png(paste(working_dir,"/images/heatmap_samples.png", sep=""))
heatmap.2(mat, Rowv=as.dendrogram(hc),
          symm=TRUE, trace='none',
          col=rev(hmcol),margin=c(10, 10), colRow=colo, colCol=colo)
dev.off()

log2.norm.counts <- assay(nt)[select,]
pheatmap(log2.norm.counts,
         clustering_distance_cols = dist1, 
         clustering_method = "ward.D", 
         cluster_rows=FALSE,
         cluster_cols=TRUE, 
         annotation_col=df,
         show_rownames=TRUE,
         fontsize_row=5,
         fontsize_col=10,
         fontsize=8,
         annotation_colors = ann_colors, 
         main=paste("Clustered heatmap of 100 most abundant genes\n",dist1," distance with ",clust, " clustering method",sep=""),
         las=1,
         filename=paste(working_dir,"/images/heatmap_expression.png", sep=""))

png(paste(working_dir,"/images/dispersion_estimates.png", sep=""))
plotDispEsts(dds)
dev.off()

png(paste(working_dir,"/images/MA-plot_without-lfc-shrinkage.png", sep=""))
plotMA.DESeqResults(res_raw, main="Acidity vs. Control\nbefore lfc shrinkage", ylim=c(-10,10),alpha=0.05)
dev.off()

png(paste(working_dir,"/images/MA-plot_with-lfc-shrinkage.png", sep=""))
plotMA.DESeqResults(res, main="Acidity vs. Control\after apeglm shrinkage", ylim=c(-10,10),alpha=0.05)
dev.off()

write.table(resag, paste(working_dir,"/all_genes.txt", sep=""), quote=F, row.names=F, sep="\t")
write.table(res0.01ag, paste(working_dir,"/DE-genes_acid-vs-control_pv0.01.txt", sep=""), quote=F, row.names=F, sep="\t")
write.table(res0.05ag, paste(working_dir,"/DE-genes_acid-vs-control_pv0.05.txt", sep=""), quote=F, row.names=F, sep="\t")
write.table(res0.01_1.5ag, paste(working_dir,"/DE-genes_acid-vs-control_pv0.01_fc1.5.txt", sep=""), quote=F, row.names=F, sep="\t")
write.table(res0.05_1.5ag, paste(working_dir,"/DE-genes_acid-vs-control_pv0.05_fc1.5.txt", sep=""), quote=F, row.names=F, sep="\t")

getwd()
write.table(log2.norm.counts.to.export, paste(working_dir,"/norm-counts-matrix_design2b.txt", sep=""), quote=F, row.names=T, sep="\t")


sessionInfo()
