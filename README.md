# Effect of ocean acidification on fish liver transcriptome.  
David Mazurais et al., 2023  

## Differential analysis of RNA-seq count data from seabass (D. labrax) samples.
This repository contains scripts that have been used to analyze RNA-seq data from David Mazurais et al., 2023.  
Raw reads have been published to ENA under the accession number [PRJEB59362](https://www.ebi.ac.uk/ena/browser/view/PRJEB59362).   

The dataset is composed of 14 samples from 2 conditions : control (pH=8) and acidity (pH=7.6) and 2 sampling times (J1 and J2). The analysis was done using R language, DESeq2 package and SARTools.


# Repository structure

**20220202_RNA-seq**   
├── **data**              *input data needed for the analyses, except for raw fastq files*  
├── **env**               *conda environment files (yml format) used for DEGs analysis*  
├── **results**           *some results from the analysis*  
│   ├── ***DEG***           *lists of DEGs*   
│   ├── ***go_analysis***   *results tables from GO analysis with goatools*  
│   ├── ***star***          *count tables from STAR*    
└── **src**               *scripts run to perform the analysis, the order is indicated by the prefix in the script names*  


# Additional information
To run the scripts, you will need to set the following variables :
- `APPLI`: Path to repository containing tools conda environments
- `LOGDIR`: Path to folder which will contain log files from the scripts
- `DATA`: Path to folder containing input data
- `OUTPUT` : Path to output directory
- `GENOMES` : Path to folder containing D. labrax reference genome AND star index.

Juptyer notebook HTML file containing differential analysis is located in `src/06_DE-analysis`   

# Material and methods
Read quality was assessed with fastqc v0.11.9 (Andrews, 2010) and multiQC v1.9 (Ewels et al., 2016).
Raw reads were processed using Trim Galore v0.6.7 (Krueger, 2012) to perform quality trimming (Q=15) and to remove the T overhang at 5' end of reads, according to Illumina recommendations for stranded mRNA Prep protocol (llumina : Best practices for read trimming for illumina stranded mrna and total rna workflows, 2020).   
  
Trimmed reads were mapped on Dicentrarchus labrax genome v1 (Tine et al., 2014) using STAR v2.7.9a (Dobin et al., 2013) with standard parameters. STAR outputs matrices of raw counts which were used to perform differential analysis.   
  
To minimize the false-positive rate, the count matrices were filtered for low expressed transcripts (minimum counts>10 in at least half of the samples). Differentially expressed genes (DEGs) between acidic and control samples were identified using DESeq2 v2.1.34 (Love et al., 2014) with [R](https://www.R-project.org/) v4.1.2 and a collection of graphics and statistics packages : rafalib (Irizarry et al., 2015) ; pheatmap (Kolde, 2019) ; Sartools (Varet et al., 2016) ; gplots (Warnes et al., 2020) and ggplot2 (Wickham, 2016). DESeq2 method internally corrects for library size and uses negative binomial generalized linear models to test for differential expression. DEGs were annotated using available annotations published with GCA_000689215.1 genome release (Tine et al., 2014).   
  
In this study the statistical design considered two factors: pH condition and sampling day. The statistical model was built using 'counts ~ sampling_day + group' design formula, where group qualitative variable indicates sample group (acidity/control). Fold changes were adjusted using bayesian shrinkage estimators for effect sizes with apeglm package (Zhu et al., 2019). All features with adjusted p-value smaller than 0.05 (Benjamini-Hochberg method) were reported as differentially expressed (DEGs).  
  
Over- and under-representation of Gene Ontology (GO) terms associated with DEGs were tested using goatools v0.8.12 (Klopfenstein et al., 2018). Goatools process is based on Fisher's exact test and GO database from geneontology ([go-basic.obo release 2017-07-07](https://geneontology.github.io/docs/download-ontology/)).

# References
- Alexander Dobin, Carrie A. Davis, Felix Schlesinger, Jorg Drenkow, Chris Zaleski, Sonali Jha, Philippe Batut, Mark Chaisson, and Thomas R. Gin- geras. Star: Ultrafast universal rna-seq aligner. Bioinformatics, 29:15–21, 1 2013.  
- Anqi Zhu, Joseph G. Ibrahim, and Michael I. Love. Heavy-tailed prior distributions for sequence count data: Removing the noise and preserving large differences. Bioinformatics, 35:2084–2092, 6 2019  
- Felix Krueger. Babraham bioinformatics - [trim galore!](https://www.bioinformatics.babraham.ac.uk/projects/trim_galore/) 
- Gregory R. Warnes, Ben Bolker, Lodewijk Bonebakker, Robert Gentleman, Wolfgang Huber, Andy Liaw, Thomas Lumley, Martin Maechler, Arni Mag- nusson, Steffen Moeller, Marc Schwartz, and Bill Venables. gplots: Various r programming tools for plotting data, 2020.  
- Hadley Wickham. ggplot2: Elegant Graphics for Data Analysis. Springer- Verlag New York, 2016.  
- Hugo Varet, Loraine Brillet-Guguen, Jean Yves Coppe, and Marie Agns Dillies. Sartools: A deseq2- and edger-based r pipeline for comprehensive differential analysis of rna-seq data. PLoS ONE, 11, 6 2016.  
- Illumina. Best practices for read trimming for illumina stranded mrna and total rna workflows, 2020.  
- Klopfenstein DV, Zhang L, Pedersen BS, Tang H. GOATOOLS: A Python library for Gene Ontology analyses Scientific reports (2018) 8:10872 
- Mbaye Tine, Heiner Kuhl, Pierre Alexandre Gagnaire, Bruno Louro, Er- ick Desmarais, Rute S.T. Martins, Jochen Hecht, Florian Knaust, Khalid Belkhir, Sven Klages, Roland Dieterich, Kurt Stueber, Francesc Pifer- rer, Bruno Guinand, Nicolas Bierne, Filip A.M. Volckaert, Luca Bargel- loni, Deborah M. Power, Francois Bonhomme, Adelino V.M. Canario, and Richard Reinhardt. European sea bass genome and its variation provide insights into adaptation to euryhalinity and speciation. Nature communi- cations, 5:5770, 2014.  
- Michael I. Love, Wolfgang Huber, and Simon Anders. Moderated estima- tion of fold change and dispersion for rna-seq data with deseq2. Genome Biology, 15, 12 2014.  
- Philip Ewels, Mns Magnusson, Sverker Lundin, and Max Kller. Multiqc: Summarize analysis results for multiple tools and samples in a single report. Bioinformatics, 32:3047–3048, 10 2016.  
- Rafael A. Irizarry and Michael I. Love. rafalib: Convenience functions for routine data exploration, 2015.  
- Raivo Kolde. pheatmap: Pretty heatmaps, 2019.  
- Simon Andrews. Babraham bioinformatics - [fastqc](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/
) : A quality control tool for high throughput sequence data. 

